#!/usr/bin/env python3
import evdev
from colorsys import hsv_to_rgb

from NetPixel import NetPixel

def clamp(val, min_v, max_v):
    return max(min_v, min(val, max_v))

device = evdev.InputDevice("/dev/input/by-id/usb-Contour_Design_ShuttleXpress-event-if00")
device.grab()

WHEEL_LAST_VAL = 0
CURRENT_FIELD = 0
CURRENT_HSV = [0, 0, 0]

strip = NetPixel("192.168.1.130")

for e in device.read_loop():
    if e.code == 7:
        if WHEEL_LAST_VAL == 0:
            WHEEL_LAST_VAL = e.value

        if e.value == WHEEL_LAST_VAL:
            continue

        if e.value > WHEEL_LAST_VAL or (WHEEL_LAST_VAL == 255 and e.value == 1):
            CURRENT_HSV[CURRENT_FIELD] = clamp(CURRENT_HSV[CURRENT_FIELD] + 1, 0, 100)
        elif e.value < WHEEL_LAST_VAL or (WHEEL_LAST_VAL == 0 and e.value == 255):
            CURRENT_HSV[CURRENT_FIELD] = clamp(CURRENT_HSV[CURRENT_FIELD] - 1, 0, 100)


        WHEEL_LAST_VAL = e.value
        converted_hsv = [i / 100 for i in CURRENT_HSV]
        strip.fill(tuple([int(255 * i) for i in hsv_to_rgb(*converted_hsv)]))
        strip.write()
    elif 261 <= e.code <= 263 and e.value == 0:
        CURRENT_FIELD = e.code - 261

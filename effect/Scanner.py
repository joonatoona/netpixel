#!/usr/bin/env python3
from time import sleep

try:
    import openrazer.client as rc
    RAZER_ENABLED = True
except:
    RAZER_ENABLED = False

from NetPixel import NetPixel

KEYBOARD_OFFSET = 30
STRIP_IP = "192.168.1.130"

COLOR_PHASE = 0
COLOR = [255, 0, 0]

def increment_color():
    global COLOR_PHASE
    global COLOR
    while True:
        if COLOR_PHASE == 0:
            if COLOR[0] > 0:
                COLOR[0] -= 1
                COLOR[1] += 1
                return tuple(COLOR)
            COLOR_PHASE = 1
        elif COLOR_PHASE == 1:
            if COLOR[1] > 0:
                COLOR[1] -= 1
                COLOR[2] += 1
                return tuple(COLOR)
            COLOR_PHASE = 2
        else:
            if COLOR[2] > 0:
                COLOR[2] -= 1
                COLOR[0] += 1
                return tuple(COLOR)
            COLOR_PHASE = 0

def main():
    global RAZER_ENABLED
    if RAZER_ENABLED:
        try:
            devMan = rc.DeviceManager()
            kb = devMan.devices[0]
            rows, cols = kb.fx.advanced.rows, kb.fx.advanced.cols

            coords = range(KEYBOARD_OFFSET, KEYBOARD_OFFSET + cols - 2)
        except:
            RAZER_ENABLED = False

    p = NetPixel(STRIP_IP)

    while True:
        for c in range(p.length):
            cc = increment_color()
            p.fill((0, 0, 0))
            p[c] = cc
            if RAZER_ENABLED and c in coords:
                kb.fx.advanced.matrix.reset()
                r = c - KEYBOARD_OFFSET
                for y in range(rows):
                    kb.fx.advanced.matrix[y, r] = cc
                kb.fx.advanced.draw()
            p.write()
            sleep(0.02)

        for c in range(p.length - 1, 0, -1):
            cc = increment_color()
            p.fill((0, 0, 0))
            p[c] = cc
            if RAZER_ENABLED and c in coords:
                kb.fx.advanced.matrix.reset()
                r = c - KEYBOARD_OFFSET
                for y in range(rows):
                    kb.fx.advanced.matrix[y, r] = cc
                kb.fx.advanced.draw()
            p.write()
            sleep(0.02)

if __name__ == "__main__":
    main()

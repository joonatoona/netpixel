#!/usr/bin/env python3
from time import sleep

from NetPixel import NetPixel

strip = NetPixel("192.168.1.130")

while True:
    for r in range(255):
        strip.fill((r, 0, 0))
        strip.write()
        sleep(0.01)
    for r in range(255, 0, -1):
        strip.fill((r, 0, 0))
        strip.write()
        sleep(0.01)
